## TODO list

1. check if long_term_tracking() is working well 
2. make more modular long_term_tracking() method
3. add more attributes like
	1. bbox
	2. TDOT/Auxilary network
	3. ....
4. Add gt conversion class


## Credits

KCF code is based: https://github.com/fengyang95/pyCFTrackers

SiamFC code and the weights are based on :https://github.com/huanglianghua/siamfc-pytorch