import os
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt
import argparse


import shutil

import time

from mrcnn import utils
# import mrcnn.model as modellib
# import mrcnn.model as modellib
import mrcnn.model_case1 as modellib
import mrcnn.visualize
from mrcnn.config import Config

# from kcf.kcf_particle_full_version import KCF
from kcf.kcf_particle_full_version_1 import KCF
import cv2 

from lbp import lbp_decimal_8
from lbp import chi_square_dist
from lbp import L2_dist
from lbp import resize_bbox



class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
               'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']


target_class = "trainnn"

target_id = -1
for i, class_name in enumerate(class_names):
    if class_name == target_class:
        target_id = i
        break


target_class_names_dict = {}
# print("type: ",type(ARG_TARGET_CLASS_NAME_FILE))

#print("\n\nFrom script: ")
#print(ARG_TARGET_CLASS_NAME_FILE)
#print("\n",ARG_IMAGE_DIR)

#ARG_TARGET_CLASS_NAME_FILE='C:\\Users\\eksamin4\\Desktop\\PhD\\Research\\Correlation_Filter_based_Tracking\\Code\\TDOT\\target_class_file.txt'
#ARG_IMAGE_DIR = "'C:\\Users\\eksamin4\\Desktop\\PhD\\Research\\Correlation_Filter_based_Tracking\\Code\\TDOT\\data\\'"

#print("\nManually: ")
#print(ARG_TARGET_CLASS_NAME_FILE)
#print("\n",ARG_IMAGE_DIR)

ARG_TARGET_CLASS_NAME_FILE = "C:\\Users\\llukm\\Desktop\\TDOT\\target_class_file.txt"

if ARG_TARGET_CLASS_NAME_FILE is not None:

    assert os.path.exists(ARG_TARGET_CLASS_NAME_FILE), "Target class name file does not exist on path: {}".format(ARG_TARGET_CLASS_NAME_FILE)
    with open(ARG_TARGET_CLASS_NAME_FILE,'r') as f:
        for line in f:
            tokens = line.split('=')
            tokens[1] = tokens[1].replace('\n','')
            assert tokens[1] in class_names, "Target class name '{}' is not included for video '{}'".format(tokens[1],tokens[0])
            target_class_names_dict[tokens[0]] = tokens[1]
# endregion [COCO class name init & target validation] --

PARTICLE_COUNT = 200


ARG_MODE = "extension"
ARG_COCO_MODEL_PATH = "C://Users//llukm//Desktop//TDOT//Mask_RCNN_Model//mask_rcnn_coco.h5"

class InferenceConfig(Config):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    global ARG_MODE
    NAME = "coco_evaluation"
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    # NUM_CLASSES = 1 + 1
    NUM_CLASSES = 80 + 1
    DETECTION_MIN_CONFIDENCE = 0.0
    DETECTION_NMS_THRESHOLD = 0.7
    FILTER_BACKGROUND = True

    if ARG_MODE == "extension" or "rpn":
        POST_PS_ROIS_INFERENCE = PARTICLE_COUNT
        DETECTION_MAX_INSTANCES = 400
        # POST_PS_ROIS_INFERENCE = 1000
        # DETECTION_MAX_INSTANCES = 1000
    elif ARG_MODE == "inference":
        POST_PS_ROIS_INFERENCE = 1000
        DETECTION_MAX_INSTANCES = 1000
        
    INIT_BN_BACKBONE = True
    INIT_GN_BACKBONE = False
    INIT_BN_HEAD = True
    INIT_GN_HEAD = False
        

config = InferenceConfig()
#config.display()

def particle_mask_normalize(particles, first_img, config=config):
    first_img = skimage.io.imread(first_img)
    dim_y, dim_x, _ = first_img.shape

    for i in range(0,particles.shape[0]):
        # print("particle [{}] before: {}".format(i,particles[i]))
        x, y, w, h = particles[i]
        particles[i, 0] = y / dim_y
        particles[i, 1] = x / dim_x
        particles[i, 2] = (y + h) / dim_y
        particles[i, 3] = (x + w) / dim_x
        # print("particle [{}] after: {}".format(i,particles[i]))

    particles_np = np.array(particles).astype(np.float64)



    # Get the scale and padding parameters by using resize_image.
    _, _, scale, pad, _ = utils.resize_image(first_img,
                                             min_dim=config.IMAGE_MIN_DIM,
                                             max_dim=config.IMAGE_MAX_DIM,
                                             min_scale=config.IMAGE_MIN_SCALE,
                                             mode="square")

    # Roughly calculate padding across different axises.
    aver_pad_y = (pad[0][0] + pad[0][1]) / 2
    aver_pad_x = (pad[1][0] + pad[1][1]) / 2

    particles_np *= np.array((dim_y, dim_x, dim_y, dim_x))
    particles_np = (particles_np * scale) + np.array((aver_pad_y, aver_pad_x, aver_pad_y, aver_pad_x))
    particles_np /= np.array(
        (1024, 1024, 1024, 1024))  ## ozgun soru: bunun config.IMAGE_MAX_DIM vs olması gerekmiyor muydu?
    particles_np = particles_np.reshape((-1, config.POST_PS_ROIS_INFERENCE, 4))
    return particles_np


model = modellib.MaskRCNN(mode=ARG_MODE, model_dir=ARG_COCO_MODEL_PATH, config=config)

print("Loading weights")
model.load_weights(ARG_COCO_MODEL_PATH, by_name=True)

GAUSS_STDEV_COORD_INIT = 5
GAUSS_STDEV_SCALE_INIT = 5
GAUSS_STDEV_COORD = GAUSS_STDEV_COORD_INIT
GAUSS_STDEV_SCALE = GAUSS_STDEV_SCALE_INIT
STDEV_COORD_MISS_MULTIPLIER = 3  # NEW_GAUSS_STDEV_COORD = OLD_GAUSS_STDEV_COORD * STDEV_COORD_MISS_MULTIPLIER
STDEV_SCALE_MISS_MULTIPLIER = 1  # NEW_GAUSS_STDEV_SCALE = OLD_GGAUSS_STDEV_SCALE * STDEV_SCALE_MISS_MULTIPLIER


TARGET_CLASS_ID = 1


class VotGt2016Bbox:
    #def __init__(self, top_left_x, top_left_y, top_right_x, top_right_y, bottom_right_x, bottom_right_y, bottom_left_x, bottom_left_y):
    def __init__(self, tokens):
        # self.top_left_x = top_left_x
        # self.top_left_y = top_left_y
        # self.top_right_x = top_right_x
        # self.top_right_y = top_right_y
        # self.bottom_right_x = bottom_right_x
        # self.bottom_right_y = bottom_right_y
        # self.bottom_left_x = bottom_left_x
        # self.bottom_left_y = bottom_left_y

        self.top_left_x = float(tokens[0])
        self.top_left_y = float(tokens[1])
        self.top_right_x = float(tokens[2])
        self.top_right_y = float(tokens[3])
        self.bottom_right_x = float(tokens[4])
        self.bottom_right_y = float(tokens[5])
        self.bottom_left_x = float(tokens[6])
        self.bottom_left_y = float(tokens[7])

    def convert_to_mrcnn_bbox(self):
        """
        (1)----(2)
        \       \
        \       \
        (4)----(3)

        From format
            x1,y1,x2,y2,x3,y3,x4,y4
        to format
            x1,y1,w,h
        :return:
        """
        left_avg = math.floor((self.top_left_x + self.bottom_left_x) / 2)
        top_avg = math.floor((self.top_left_y + self.top_right_y) / 2)
        width_avg = math.floor(((self.top_right_x - self.top_left_x)+(self.bottom_right_x - self.bottom_left_x))/2)
        height_avg = math.floor(((self.bottom_left_y - self.top_left_y) + (self.bottom_right_y - self.top_right_y)) / 2)
        return [left_avg, top_avg, width_avg, height_avg]




def get_initial_ground_truth():
    with open("data/basketball_gt/groundtruth.txt",'r') as f:
        line = f.readline()
        tokens = line.split(',')
        tokens = list(filter(None, tokens))  # filter whitespace
        gt_bbox = VotGt2016Bbox(tokens).convert_to_mrcnn_bbox()
    return gt_bbox # x,y,w,h


gt = get_initial_ground_truth()

video_dir = "data/basketball_img/"

image_ids = os.listdir(video_dir)
sorted_image_ids = sorted(image_ids, key=lambda x: x[:-4])
sorted_image_ids = list(filter(lambda x: x[-4:] == ".jpg", sorted_image_ids))
image = skimage.io.imread(os.path.join(video_dir, sorted_image_ids[0]))


particles = np.zeros((1, PARTICLE_COUNT, 4))

total_frame_count = len(sorted_image_ids)
current_frame_index = 0
# for d, image_id in enumerate(sorted_image_ids):


def coco_to_voc_bbox_converter(y1, x1, y2, x2, roi_score=-1):
    w = x2 - x1
    h = y2 - y1
    return x1, y1, w, h, roi_score

total_frame_count = 40
while current_frame_index < total_frame_count:
    image_name = sorted_image_ids[current_frame_index]
    print(image_name)
    if current_frame_index == 0:
        current_frame_index += 1
        continue
    
    
    if (image_name[-4:] == ".jpg"):
        # print(skimage.io.imread(os.path.join(video_dir, image_id)))
        image = skimage.io.imread(os.path.join(video_dir, image_name))

#        image_list.append(image)
        dims = image.shape
        canvas_shape_x_y_format = [dims[1], dims[0]]
        
        results = model.detect([image], verbose=1, particles=particle_mask_normalize(particles[0],os.path.join(video_dir, os.listdir(video_dir)[0]),config)[0])
        print("end results")
#        results = model.detect(image_list, verbose=1, particles=particle_mask_normalize(particles[0],os.path.join(video_dir, os.listdir(video_dir)[0]),config)[0])
        
        r = results[0]
        
        score_id = 0
        total_num_candidate = r['class_ids'].size
        print("\ntotal_num_candidate\t:",total_num_candidate)
        
        y1, x1, y2, x2 = r['rois'][score_id]
        obj_score = r['scores'][score_id]
        predicted_class_id = r['class_ids'][score_id]
        
        

        x_part, y_part, w_part, h_part, score_part = coco_to_voc_bbox_converter(y1, x1, y2, x2, obj_score)
        bbox_candidate = x_part, y_part, w_part, h_part
        
        if score_id == 0:
            match_bbox_for_particle_seed = bbox_candidate
            b_box_image = cv2.rectangle(image, (x_part, y_part), (w_part+x_part, h_part+y_part), (0,0,255), 4)
        else:
            b_box_image = cv2.rectangle(image, (x_part, y_part), (w_part+x_part, h_part+y_part), (0,255,0), 4)
                
        cv2.imshow('image',b_box_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        
        
        random.seed(5)
        particles = utils.sample_bbox(match_bbox_for_particle_seed, stdev_coord=GAUSS_STDEV_COORD, stdev_scale=GAUSS_STDEV_SCALE, count=PARTICLE_COUNT, canvas_shape=canvas_shape_x_y_format)
        match_bbox_for_particle_seed_last = match_bbox_for_particle_seed
#                roi_threshold = ROI_THRESHOLD
        GAUSS_STDEV_COORD = GAUSS_STDEV_COORD_INIT
        GAUSS_STDEV_SCALE = GAUSS_STDEV_SCALE_INIT
        print("")
        print(match_bbox_for_particle_seed)
        print(particles.mean(1))
#        
        
        current_frame_index += 1